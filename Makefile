.PHONY: all build test-all test lint vet

all: init test-all build build-docker

init:
	go get -u github.com/golang/dep/cmd/dep

dep:
	dep ensure

build: dep
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

build-docker:
	docker build -t go-dummy .

test-all: vet lint test

test:
	go test -v -parallel=4 ./...

lint:
	@go get github.com/golang/lint/golint
	go list ./... | xargs -n1 golint

vet:
	go vet ./...
